package com.tnkim.jpademo.service;

import com.tnkim.jpademo.model.test.TestModel;
import com.tnkim.jpademo.model.test.TestModelRepository;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
public class TestService {
    static Logger logger = LoggerFactory.getLogger(TestService.class);

    @Autowired
    TestModelRepository testModelRepository;

    public void test() throws InterruptedException {

        TestModel newModel = new TestModel();
        newModel.setName("mac");
        newModel.setComment("no comment");
        TestModel savedModel = testModelRepository.save(newModel);
        logger.info("saved model: {}", savedModel.toString());

        new Thread(() -> {
            Optional<TestModel> optTmpModel;
            TestModel tmpModel;
            optTmpModel = testModelRepository.findById(savedModel.getId());
            if (optTmpModel.isPresent()) {
                tmpModel = optTmpModel.get();
                tmpModel.setComment("completed");
                testModelRepository.save(tmpModel);
            }
        }).start();

        Optional<TestModel> optTestModel;
        TestModel testModel;

        while(true) {
            optTestModel = testModelRepository.findById(savedModel.getId());
            if (optTestModel.isPresent()) {
                testModel = optTestModel.get();
                logger.info("found TestModel: {}", testModel.toString());
                if (testModel.getComment().equals("completed")) {
                    break;
                }
            }

            Thread.sleep(1000L);
        }

        logger.info("the comment of the testModel : {}", testModel.getComment());
    }


}

package com.tnkim.jpademo.controller;


import com.tnkim.jpademo.service.TestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.Callable;

@RestController
@RequestMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
public class TestController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    TestService testService;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public Callable<Boolean> test() {
        return () -> {
//            new Thread(() -> {
//                try {
//                    testService.test();
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }).start();
            testService.test();

            return true;
        };
    }

}

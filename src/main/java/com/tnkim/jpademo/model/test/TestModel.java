package com.tnkim.jpademo.model.test;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Cacheable
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@ToString
public class TestModel {

    /**
     * 아이디
     */
    @Id @GeneratedValue
    private Long id;

    /**
     * 이름
     */
    @Column
    private String name;

    /**
     * comment
     */
    @Column
    private String comment;
}

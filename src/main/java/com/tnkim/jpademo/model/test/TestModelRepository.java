package com.tnkim.jpademo.model.test;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TestModelRepository extends JpaRepository<TestModel, Long> {

}


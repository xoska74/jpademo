package com.tnkim.jpademo;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
@Transactional
public class JpademoRunner implements ApplicationRunner {


    @Override
    public void run(ApplicationArguments args) throws Exception {
        // do nothing

    }

}

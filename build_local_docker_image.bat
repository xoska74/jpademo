@echo off

if [%1]==[] GOTO :arg_exit

echo version: %1

REM mvn clean -DskipTests package -P dev

REM docker login
docker build -f Dockerfile -t jpademo:%1 . --build-arg JAR_FILE="jpademo-0.0.1.jar"
REM docker tag zcare-api:%1 xoska83/zcare-api:%1
REM docker push xoska83/zcare-api:%1
REM docker system prune -f
REM docker images | findstr zcare-api

exit /b

:arg_exit
echo Please input version info like 0.1.0.
